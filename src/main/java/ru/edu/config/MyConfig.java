package ru.edu.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import ru.edu.*;

@Configuration
@PropertySource("classpath:application.properties")
public class MyConfig {

    @Value("${provider.default.args}")
    private String[] arguments;

    @Bean
    public MessageProvider messageProvider() {
        return new ArgumentsMessageProvider(arguments);
    }

    @Bean
    public MessageRenderer messageRenderer() {
        JsonMessageRenderer consoleMessageRenderer = new JsonMessageRenderer();
        return consoleMessageRenderer;
    }
}
