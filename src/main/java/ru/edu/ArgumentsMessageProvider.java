package ru.edu;

public class ArgumentsMessageProvider implements MessageProvider {
    private final String message;

    public ArgumentsMessageProvider(String[] arguments) {
        for (int i = 0; i < arguments.length; ++i) {
            System.out.println("param index=" + i + " value=" + arguments[i]);
        }
        if (arguments. length < 1) {
            throw new IllegalArgumentException("Can't find str in arguments");
        }
        message = arguments[0];
    }

    @Override
    public String getMessage() {
        return message;
    }
}
