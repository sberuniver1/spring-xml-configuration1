package ru.edu;

import org.springframework.beans.factory.annotation.Autowired;

public class JsonMessageRenderer implements MessageRenderer {
    //Внедряем зависимость провайдера в наш рендерер
    private MessageProvider messageProvider;

    //Инициализируем наш провайдер
    @Override
    @Autowired
    public void setMessageProvider(MessageProvider messageProvider) {
        this.messageProvider = messageProvider;
    }

    @Override
    public void render() {
        //теперь мы можем вызвать метод getMessage у самого провайдера, и не принимать входным аргументом строку
        String json = String.format("{\"message\": \"%s\"}", messageProvider.getMessage());
        System.out.println(json);
    }
}
