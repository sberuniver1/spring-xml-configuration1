package ru.edu;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        //инициализируем контекст приложения через XML настройку. Указываем путь до нашего xml файла.
        //так как файл лежит в папке resources, то мы ставим приставку classpath
        ApplicationContext context = new AnnotationConfigApplicationContext("ru.edu");
        System.out.println("Application context initialized");
        //Получаем бин нашего провайдера из контекста.
        MessageProvider messageProvider = context.getBean(MessageProvider.class);

        //создаем бин рендерера
        MessageRenderer messageRenderer = context.getBean(MessageRenderer.class);
        //выводим на экран результат
        messageRenderer.render();
    }

}